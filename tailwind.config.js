/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts}'],
  theme: {
    colors: {
      'primary': {
        '100': '#445277',
      },
      'neutral': {
        "100": '#FCFCFC',
        "200": '#F6F6F6',
        "300": '#EEEEEE',
        "400": '#CACACA',
        "500": '#8D8D8D',
        "600": '#777777',
        "700": '#555555',
        "800": '#3C3C3C',
        "900": '#242424'
      },
      'gray': {
        "100": '#f8f9fa',
        "200": '#f2f2f2',
        "300": '#e0e0e0',
        "400": '#ced4da',
        "500": '#adb5bd',
        "600": '#6c757d',
        "700": '#495057',
        "800": '#343a40',
        "900": '#212529'
      },
      'orange': {
        "default": '#FF832B',
      },
      'danger': '#e73d4a',
      'success': '#78B862',
      'white': '#fff',
      'black': '#000',
      'dark': {
        "100": '#131A34',
      }
    },
    extend: {},
  },
  plugins: [],
}

