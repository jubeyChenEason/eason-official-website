import { createRouter, createWebHistory } from 'vue-router';
import { ROUTING_PATH } from '@/consts/routing-path.const';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
       {
            path: ROUTING_PATH.HOME,
            name: 'home',
            component: () => import('@/views/HomeView.vue'),
        },
        {
            path: ROUTING_PATH.ABOUT,
            name: 'about',
            component: () => import('@/views/AboutView.vue'),
        },
        {
            path: ROUTING_PATH.SERVICE,
            name: 'service',
            component: () => import('@/views/ServiceView.vue'),
        },
        {
            path: ROUTING_PATH.COMPANY,
            name: 'company',
            component: () => import('@/views/CompanyView.vue'),
        },
        {
            path: ROUTING_PATH.CONTACT,
            name: 'contact',
            component: () => import('@/views/ContactView.vue'),
        },
        {
            path: '/:pathMatch(.*)*',
            redirect: ROUTING_PATH.HOME
        }
       
    ]
});


export default router;
