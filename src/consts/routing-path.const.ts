export const ROUTING_PATH = {
    /** 登入頁 */
    LOGIN: '/login',
    SAML_LOGIN: '/saml/login',
    /** 首頁 */
    HOME: '/home',
    /** 關於毅聲 */
    ABOUT: '/about',
    /** 產品服務 */
    SERVICE: '/service',
    /** 企業永續 */
    COMPANY: '/company',
    /** 聯絡我們 */
    CONTACT: '/contact',
};
